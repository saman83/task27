const express = require("express");
const app = express();
const path = require('path');
const cookieParser = require('Cookie-parser');
const session = require('express-session');

//Enable request cookies
app.use(cookieParser());

//Enables Server sessions
app.use(session({
    secret: 'asd123=',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false, //Allows sessions to work on HTTP
        httpOnly: true, //  Disables cookie for document.cookie
        maxAge: 36000000 //1 hour
    }
}))

//Serve my static content (js, css, images)
app.use('/public', express.static(path.join(__dirname, 'www')));



// app.get('/', (req, res) => {
//     if (req.session.counter == undefined) {
//         res.send('No sessions exists for this user...');
//     } else {
//         req.session.counter += 1;
//         res.send('Number of site visits for this user: ' + req.session.counter);
//     }
// });


app.get('/', (req, res) => {
    if (req.session.user == undefined) {
        res.redirect('/login');
        res.send('No sessions exists for this user...');
    } else {
        res.redirect('/dashboard');
    }
});

app.get('/create', (req, res) => {
    req.session.user = JSON.stringify({
        name: 'Saman'
    })
    res.send('Session created..')
})

app.get('/dashboard', (req, res) => {
    res.send('Task27 - You have bee redirected to /dashboard :)')
})

app.get('/login', (req, res) => {
    req.session.counter = 0;
    res.send('Task27 - No sessions existed, because you are in localhost:3000/Login')
})

app.listen(3000, () => console.log('App started on Port 3000...'));